
const socket = io();
console.log("socket.io loaded");


// ----- parse the outputs

function parse_nextcloud(out){
    let lines = out.split('\n');
    let friendly = [];

    friendly.push("[Changes]")
    let changes = false;
    for (let i = 0; i < lines.length; i++) {
        let [line_header, line_data] = lines[i].split('\t');
        // is an update info
        if( line_header.includes("[ info nextcloud.sync.csync.updater ]")){
            // is eval by nextcloud and is a file
            if( line_data.includes("INSTRUCTION_EVAL") && line_data.includes(".") ){
                friendly.push(line_data.split(',')[0]);
                changes = true;
            }
        }
    }
    if(!changes){
        friendly.push("no change")
    }

    return friendly.join('\n');
}

function parse_stdout(name, stdout){
    if(name == "nextcloud"){
        return parse_nextcloud(stdout);
    }
}


// ----- print to html stdout

function empty_stdout(name){
    let section = document.getElementById(name+"-section");
    let terminal_output = document.getElementById("stdout-"+name);
    let friendly_terminal_output = document.getElementById("stdout-"+name+"-friendly");

    // default
    terminal_output.innerHTML = "";
    
    // friendly
    friendly_terminal_output.innerHTML = "";

    section.classList.add("running");
}

function write_stdout(name, error, stdout, stderr){
    let section = document.getElementById(name+"-section");
    let terminal_output = document.getElementById("stdout-"+name);
    let friendly_terminal_output = document.getElementById("stdout-"+name+"-friendly");

    // default
    terminal_output.innerHTML = stdout;
    terminal_output.innerHTML += "<hr/>"
    terminal_output.innerHTML += error;
    terminal_output.innerHTML += "<hr/>"
    terminal_output.innerHTML += stderr;
    let total_stdout = stdout + error + stderr;

    // friendly
    friendly_terminal_output.innerHTML = parse_stdout(name, total_stdout);

    section.classList.remove("running");
    section.classList.add("done");
}


// ----- send command to execute

function nextcloudSync(){
    empty_stdout("nextcloud");
    socket.emit("exec nextcloud", socket.id);
}
function gitlabPull(){
    empty_stdout("gitlab");
    socket.emit("exec gitlab", socket.id);
}
function pelicanGen(){
    empty_stdout("pelican");
    socket.emit("exec pelican", socket.id);
}


// ----- we recieve the command line output

socket.on("stdout", (name, error, stdout, stderr) => {
    write_stdout(name, error, stdout, stderr);
});
