const express = require('express');
const app = express();
const httpServer = require('http').Server(app);
const { Server } = require('socket.io');
const exec = require('child_process').exec;


// VARIABLES
// ------------------------------------------------------------------------

const hostname = "localhost";
const port = process.env.NODE_PORT || 3000;


// SERVER
// ------------------------------------------------------------------------

app.use(express.static('client'));

const io = new Server(httpServer, {
    // we have to handle CORS as the client is not on the same domain
    // than those script (see client-svg.js)
    // https://socket.io/docs/v3/handling-cors/
    // https://github.com/expressjs/cors#configuration-options
    // cors: {
    //   origin: allowed_clients,
    //   methods: ["GET"]
    // }
});


// EXECUTE SERVER SIDE SCRIPTS
// ------------------------------------------------------------------------


function execute(command, callback){
    exec(command, function(error, stdout, stderr){ 
        callback(error, stdout, stderr); 
    });
};


// SOCKETS
// ------------------------------------------------------------------------

io.on("connection", (socket) => {

    console.log("new connection", socket.id);
    
    socket.on("exec nextcloud", () => {
        let command = "../nextcloudsync.sh";
        let name = "nextcloud";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

    socket.on("exec gitlab", () => {
        let command = "../gitlabpull.sh";
        let name = "gitlab";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

    socket.on("exec pelican", () => {
        let command = "../pelicangen.sh";
        let name = "pelican";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

});

// CREATE LISTEN
// ------------------------------------------------------------------------

httpServer.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);

    execute
});